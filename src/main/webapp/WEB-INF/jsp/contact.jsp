<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<head>
    <title>Register and G.R.O.W.</title>
    
    <!--  vars -->
    <c:set var="path" value="${pageContext.request.contextPath}"/>
    <c:set var="host" value="${pageContext.request.serverName}"/>

    <!--  CSS etc -->
    <link href="<c:url value="/resources/css/all.css"/>" rel="stylesheet" type="text/css" />



</head>
<body id="body">

    <!-- Part 1: Wrap all page content here -->
    <div id="wrap">


 <jsp:include page="/WEB-INF/jsp/header/header.jsp"></jsp:include>


<!--  MAIN CONTAINER FOR ALL , need wrap class for footer and css working tricks -->
<div class="container main4all">




<div id="contact" class="offset2 span8">


<h4>Contact form to admin with suggestions, bugs and etc. ...
</h4>
<br />

<c:set var="mailOK" value="${mailOK}" />
 <c:choose> 
      <c:when test="${mailOK!=null}">
            <div class="alert alert-success"> ${mailOK} </div>
      </c:when>
      <c:otherwise>
            <div class="alert alert-error"> No mail send. </div>
      </c:otherwise>
</c:choose>


<br/>
<form:form method="POST" action="${path}/contact/add" modelAttribute="comment" 
     id="addCommentForm" class="addCommentForm">
        <table ><tr><td align="center">
                    <form:label path="nick">Nick</form:label>
                    <form:input path="nick"/>
                    
                    <form:label path="mail">Mail</form:label>
                    <form:input path="mail"/>
                
                    <form:label path="content">Content</form:label>
                    <form:textarea path="content" class="comment" rows="4" cols="60" /><br/>
                *Only approved comments will be shown    
                </td></tr><tr><td align="center">
                <!--  hidden -->

                <!-- errors -->
                <form:errors path="nick"     cssClass="error"/><br />
                <form:errors path="mail"     cssClass="error"/><br />
                <form:errors path="content"  cssClass="error"/><br />
                <input type="submit" class="btn btn-success" value="Send" id="submit"/>
        </td></tr></table>
</form:form>


<br/>

<!--  loading comments -->

<script>
$(document).ready(function() {
$("#commentsDiv").load("${path}/show/comments");  
});
</script>

<div id="commentsDiv"></div>





<!--  end register form      -->
</div>


<!--  end main container  -->
</div>

<!--  end wrap -->
</div>



<!--  footer , leave it -->

 <jsp:include page="/WEB-INF/jsp/footer/footer.jsp"></jsp:include>




</body>
</html>
