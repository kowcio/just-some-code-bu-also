<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

    <!--  vars -->
    <c:set var="path" value="${pageContext.request.contextPath}"/>
    <c:set var="host" value="${pageContext.request.serverName}"/>

      <!-- Begin page content -->
      <div class="container main4all">
    
      
       <div class="page-header">
          <h2>Develope Your GROW and thrive ! </h2>
          </div>

<p class="lead">
This is the main page when You can describe Your G.R.O.W. steps, print the chart and measure Your progress.</br>
<h4><code class="text-center">Remember ! You can do it !</code></h4>
 
<p class="text-info">* For not logged users the input data is stored locally (on Your computer) in cookies.</p> 
 
 </p>
  
      

        
<!--  MENU temporaryly for not logged -->
<jsp:include page="/WEB-INF/jsp/menu/menu_4all_slider_acc.jsp"></jsp:include>
    
            
      
      
      
      
      
<form:form method="POST" action="${path}/growadd" modelAttribute="grow">


   <div id="sT_goal" class="sidesText">
   As You can see there are <span class="badge badge-success">4</span>  fields You will need to fill up to complete
   Your first <span class="badge badge-info">G.R.O.W.</span> chart and print it !
   <br /><br />
   They will help You establish the goals, motives and ways to fulfill Your desires, dreams
   and ideas.
   <br /><br />
   To write down Your way and  <span class="badge badge-warning">thrive</span>  
   there are help buttons on the right.
   <br /><br />
   <span class="badge badge-success">Print </span> button redirects to a print template.<br />
   Just press ctrl + p .
   <br /><br />
   
   </div>
                                 
<div class="item" id="g1">

<img src="${path }/resources/img/frames/pink_frame1.jpg" class="frame" />
	<div class="inside_item">
	   <div class="item_header">
	              <h1>Step first : Your GOAL </h1>
	              <p class="lead">
	              Here You have the space to describe Your GOAL.
	                <br /> Be precise, use the S.M.A.R.T. rules under help button.
	               </p>     

        </div>	             
	            
<form:textarea path="grow_g" class="growtext" rows="10" cols="150"/>

	</div>
</div>
<div class="item" id="g2">
<img src="${path }/resources/img/frames/green_frame.jpg" class="frame" />
    <div class="inside_item">
               <h1>Step second : Your REALITY</h1>
              <p class="lead">
              What is Your current reality ? Do not dream, be as much objective as possible.
              <br/>  Be honest.
                   
                </p>
                     <form:textarea path="grow_r" class="growtext" rows="10" cols="150"/>
            
            
        </div>  
</div>
<div class="item" id="g3">
<img src="${path }/resources/img/frames/blue_frame.jpg" class="frame" />
    <div class="inside_item">
              <h1>Step third : Your Options </h1>
              <p class="lead">
              Think about Options / Opportunities / Obstacles , describe them, use whatever You can or have.
              Write about those that can have the most impact on Your idea.
            </p>
                  <form:textarea path="grow_o" class="growtext" rows="10" cols="150"/>
        
         
</div></div>
<div class="item" id="g4">
<img src="${path }/resources/img/frames/yellow_frame1.jpg" class="frame" />
    <div class="inside_item">
                <h1>Last step : What Will You Do ? </h1>
              <p class="lead">
              Make a promise to Yourself about things You will do, make a list.
              The chart table (on print) will help You measure Your progress.
              </p>
              <form:textarea path="grow_w" class="growtext" rows="10" cols="150"/>
 </div></div>
   
<!-- 
<input type="submit" id="submit" class="btn btn-large btn-success span5 offset3 saveBtn" value="Generate Chart" />
-->
</form:form>      
                            
      
      </div>







