<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html;charset=UTF-8"%>
    
<br/>
    
   <div class="growsDiv"> 
   
   <div class="singleGrowDiv addGrowdiv pull-left">
   <button id="addGrowBtn" class="btn btn-large btn-primary addGrow" type="button">Add Grow</button>
   </div>
   
   <c:forEach var="grow" items="${growsList}">
        <div class="singleGrowDiv pull-right">
        
        <span class="growID growIDLoadSpan text-left">${grow.id}</span>
        
        <div class="btn-group btn-group-vertical pull-right">
	        <button id="loadGrowBtn" class="btn btn-mini btn-success pull-right loadGrow lgpBtn" type="button" style="width:100%;">Load Grow</button>
	        <a href="/print/${grow.id}" id="printGrowBtn" class="btn btn-mini btn-info pull-right printGrow lgpBtn" type="button">Print Grow</a>
	        <button id="delGrowBtn" class="btn btn-mini btn-danger pull-right delGrow lgpBtn" type="button">Delete Grow</button>
        </div>
        </div>    
    </c:forEach>
   
    </div>
    
    
    
    