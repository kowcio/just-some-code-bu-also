<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

    <!--  vars -->
    <c:set var="path" value="${pageContext.request.contextPath}"/>
    <c:set var="host" value="${pageContext.request.serverName}"/>

      
      
<div id="sideBtns" class="sideBtns">      

      <!--  HELP BUTTONS -->     
      <button class="btn btn-large btn-info" id="menu">MENU</button>
<div id="menudiv" class="">
      <a class="btn btn-large btn-info btn-help"   id="helpBtnMainMain" data-placement="left" data-toggle="popover" data-container= "body">  General help </a>
      <a class="btn btn-large btn-info btn-help helpBtnG" id="helpBtnG" data-placement="left" data-toggle="popover" data-container= "body">  G  </a>
      <a class="btn btn-large btn-info btn-help helpBtnR" id="helpBtnR" data-placement="left" data-toggle="popover" data-container= "body">  R  </a>
      <a class="btn btn-large btn-info btn-help helpBtnO" id="helpBtnO" data-placement="left" data-toggle="popover" data-container= "body">  O  </a>
      <a class="btn btn-large btn-info btn-help helpBtnW" id="helpBtnW" data-placement="left" data-toggle="popover" data-container= "body">  W  </a>
      <a class="btn btn-large btn-info btn-help " href="${path}/print"> Print  </a>
      <a class="btn btn-large btn-info btn-help" href="${path}/about"> About  </a>
      <a class="btn btn-large btn-info btn-help" href="${path}/contact"> Contact  </a>
</div>    


<script>
$(document).ready(function(){


$("body").on("mouseenter", "#menu", function(event){
       $('#menudiv').stop(true,false).animate({opacity:1}, 1000);
});
$("body").on("mouseleave", "#menu", function(event){
       $('#menudiv').delay(2000).animate({opacity:0}, 2000);
});

$("body").on("mouseenter", "#menudiv", function(event){
       $('#menudiv').stop(true,false).animate({opacity:1}, 1000);
}); 
    
$("body").on("mouseleave", "#menudiv", function(event){
       $('#menudiv').delay(2000).animate({opacity:0}, 2000);
}); 

});


 /* <!-- draggable testing , ni hu hu-->
$(document).ready(function(){
    $( "#helpBtnMainMain" ).on('click',   $( "#popHelpNoLogin" ).draggable()   );
    });
)
    */


//popovers for help

$(document).ready(function(){
    $('#helpBtnMainMain').popover({ 
      html : true,
      content: $('.grow_desc').html(),
      title : 'Grow general help',  
      template: 
      '<div id="popHelpNoLogin" class="popover popHelpNoLogin ui-widget-content">'+
      '<div class="popover-inner">'+
      '<h3 class="popover-title"></h3>'+
      '<div class="popover-content"><p></p></div></div></div>'      
    });    }); 
    
$(document).ready(function(){
      $('#helpBtnG').popover({ 
        html :  true,
        content: $('.goal_help').html(),
       title :$('#goal_help_title').html(),
       template: '<div id="popHelpNoLogin" class="popover popHelpNoLogin">'+
        '<div class="popover-inner">'+
        '<h3 class="popover-title"></h3>'+
        '<div class="popover-content"><p></p></div></div></div>'      
      });   }); 

$(document).ready(function(){
    $('#helpBtnR').popover({ 
      html :  true,
      content: $('.reality_help').html(),
     title :$('#reality_help_title').html(),
     template: '<div id="popHelpNoLogin" class="popover popHelpNoLogin">'+
      '<div class="popover-inner">'+
      '<h3 class="popover-title"></h3>'+
      '<div class="popover-content"><p></p></div></div></div>'      
    });   }); 

$(document).ready(function(){
    $('#helpBtnO').popover({ 
      html :  true,
      content: $('.options_help').html(),
     title :$('#options_help_title').html(),
     template: '<div id="popHelpNoLogin" class="popover popHelpNoLogin">'+
      '<div class="popover-inner">'+
      '<h3 class="popover-title"></h3>'+
      '<div class="popover-content"><p></p></div></div></div>'      
    });   }); 

$(document).ready(function(){
    $('#helpBtnW').popover({ 
      html :  true,
      content: $('.will_help').html(),
     title :$('#will_help_title').html(),
     template: '<div id="popHelpNoLogin" class="popover popHelpNoLogin">'+
      '<div class="popover-inner">'+
      '<h3 class="popover-title"></h3>'+
      '<div class="popover-content"><p></p></div></div></div>'      
    });   }); 

</script>



</div>     
<!--  end help butons div  -->   





