<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<head>
    <title>Register and G.R.O.W.</title>
    
    <!--  vars -->
    <c:set var="path" value="${pageContext.request.contextPath}"/>
    <c:set var="host" value="${pageContext.request.serverName}"/>

    <!--  CSS etc -->

</head>
<body id="body">

    <!-- Part 1: Wrap all page content here -->
    <div id="wrap">


 <jsp:include page="/WEB-INF/jsp/header/header.jsp"></jsp:include>


<!--  MAIN CONTAINER FOR ALL , need wrap class for footer and css working tricks -->
<div class="container main4all">




<div id="about" class="offset2 span8">
<h4>
The site is created to help every person to thrive and be the best as they can and as good as they can.
It`s about beliving we can achieve anything.
</h4>
<br/>
<div class="well">
Our deepest fear is not that we are inadequate. Our deepest fear is that we are powerful 
beyond measure. It is our light, not our darkness that most frightens us.' 
We ask ourselves, Who am I to be brilliant, gorgeous, talented, and fabulous?
 Actually, who are you not to be? You are a child of God. Your playing small does not serve the world.
  There is nothing enlightened about shrinking so that other people will not feel
   insecure around you. We are all meant to shine, as children do.
    We were born to make manifest the glory of God that is within us. 
    It is not just in some of us; it is in everyone and as we let our own light shine, 
    we unconsciously give others permission to do the same. As we are liberated from 
    our own fear, our presence automatically liberates others. 
<br/><br/>
<div class="pull-right"> <i>- Marianne Williamson </i></div>
<br />    
</div>
<br/>
About the <a href="https://en.wikipedia.org/wiki/GROW_model">G.R.O.W.</a> You can read on Wikipedia.




<!--  end register form      -->
</div>


<!--  end main container  -->
</div>

<!--  end wrap -->
</div>



<!--  footer , leave it -->

 <jsp:include page="/WEB-INF/jsp/footer/footer.jsp"></jsp:include>




</body>
</html>
