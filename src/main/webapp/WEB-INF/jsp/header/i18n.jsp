<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

    <!--  vars -->
    <c:set var="path" value="${pageContext.request.contextPath}"/>
    <c:set var="host" value="${pageContext.request.serverName}"/>




                  
                  

            <!--  LANGUAGES  -->

<div id="langSelector" class="pull-right">
<a href="?language=pl" id="lang_pl"><img src="${path}/resources/img/flag_pl_small.png" width="25" height="25" alt="PL"/></a>
<a href="?language=eng" id="lang_eng"><img src="${path}/resources/img/flag_uk_small.png" width="25" height="25" alt="UK"/></a>

<spring:message code="header.current" text=" => " />

<c:set var="localeCode" value="${pageContext.response.locale}" />
 <c:choose> 
      <c:when test="${localeCode=='pl'}">
            <img src="${path}/resources/img/flag_pl_small.png" width="25" height="25" alt="PL"/>
      </c:when>
      <c:otherwise>
            <img src="${path}/resources/img/flag_uk_small.png" width="25" height="25" alt="UK"/>
      </c:otherwise>
</c:choose>


<!--  LAYOUT CHANGING skins etc -->
        <span class="lightLayout">Light |</span>
        <span class="darkLayout">Dark</span>
    
</div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
            