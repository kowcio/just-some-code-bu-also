<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 
    <!--  vars -->
    <c:set var="path" value="${pageContext.request.contextPath}"/>
    <c:set var="host" value="${pageContext.request.serverName}"/>
 

 <sec:authorize access="isAuthenticated()" var="isAuthenticated" />
 
<c:if test="${not isAuthenticated}">    
 <jsp:include page="/WEB-INF/jsp/header/header4all.jsp"></jsp:include>
 </c:if>

    
<c:if test="${isAuthenticated}">
 <jsp:include page="/WEB-INF/jsp/header/header4logged.jsp"></jsp:include>
 </c:if>
 
 
 
 
 
 
 
 
 
 
 
 
 