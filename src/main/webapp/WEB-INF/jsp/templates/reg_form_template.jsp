<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<title>Register and G.R.O.W.</title>
	
	<!--  vars -->
    <c:set var="path" value="${pageContext.request.contextPath}"/>
    <c:set var="host" value="${pageContext.request.serverName}"/>

    <!--  CSS etc -->
    <link href="<c:url value="/resources/css/all.css"/>" rel="stylesheet" type="text/css" />

</head>
<body id="body">

    <!-- Part 1: Wrap all page content here -->
    <div id="wrap">


 <jsp:include page="/WEB-INF/jsp/header/header.jsp"></jsp:include>


<!--  MAIN CONTAINER FOR ALL , need wrap class for footer and css working tricks -->
<div class="container main4all">




<div id="registerForm" class="offset2 span8">
<br />
<h2>Register a user </h2>
    
	<form:form method="post" action="reguser" modelAttribute="user">

               
		<table>	<tr>		
			<td>
			<form:input type="text" path="login"  name="login" value="${login}" placeholder="Login"/>
			</td>
			<td> 	
			<form:input type="password" path="passwd" name="passwd" value="${passwd}"  placeholder="Password"/>						
			</td>
			</tr>
			<tr>	
			<td colspan="2" align="center">
						<input type="submit" value="Register" class="btn btn-success"/>
					</td>	</tr>
					</table>
					    <div class="allOK"> ${allOK} </div>
                        <div class="error"> ${allNotOK} </div>
					<form:errors path="login" 	cssClass="error"/><br />
					<form:errors path="passwd"	cssClass="error"/><br />		
</form:form>




<!--  end register form      -->
</div>


<!--  end main container  -->
</div>

<!--  end wrap -->
</div>



<!--  footer , leave it -->

 <jsp:include page="/WEB-INF/jsp/footer/footer.jsp"></jsp:include>




</body>
</html>
