<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html;charset=UTF-8"%>
    

<!--  set values for forms on the server root adress and context path (WebApp name) -->
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>


	
<div class="displayComments">


			<c:forEach var="comment" items="${commentList}">
					<div class="well commentsWellWidth">
					<div class="commentID">
					 		${comment.id} 	
					</div><div class="commentNick">
					 		<i class="icon-user" ></i> ${comment.nick}
					 		<br />
							<i class="icon-envelope" ></i> <a href="mailto:${comment.mail}">${comment.mail}</a>
					</div>
					<div class="commentContent">
					 	  	 ${comment.content} 	
					</div>

					</div>
			</c:forEach>

</div> <!--  the posts div -->	




