package grow.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

/**
 * Post entity with fields <br />
 * id, title, content, add date
 * @author TalentLab1
 *
 */
@Entity
@Table(name="GROW_COMMENT")
public class Comment {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	@Column( unique = true, nullable = false)  
	int id;
	
	@NotNull(message="Comments cannot be empty.")
	@Length(min = 5, max = 300,message="Content length min 5 max 300 chars")
	@Column( unique = false, nullable = false)  
	String content;
	
	@NotNull(message="Nick cannot be empty.")
	@Column( unique = false, nullable = false)  
	@Length(min = 5, max = 30,message="Nick length minimum 5 chars, max 30.")
	String nick;
	
	//people often use [ a t ] as bot protection 
	/*@Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",
			message = "Bad email")*/
	@Column( unique = false, nullable = false)
	String mail;

	/**0 - to approve by admin
	 * 1 - public
	 * 2+ - other FFR
	 */
	@Column( unique = false, nullable = false)
	int viewType;
	/**
	 * language of the if while posted
	 */
	@Column( unique = false, nullable = false)
	String lang;
	
	//constructors
	
	public Comment(){
		id = 0;
		content="";
		nick = "";
		mail = "";
		viewType=0;
		lang = "";
	}
	
	public Comment(int id, String content, String nick, 
			String mail, int postID, int viewType, String lang) {
		super();
		this.id = id;
		this.content = content;
		this.nick = nick;
		this.mail = mail;
		this.viewType = viewType;
		this.lang = lang;
	}

	
	
	

	
	@Override
	public String toString() {
		return "Comment [id=" + id + ", content=" + content + ", nick=" + nick
				+ ", mail=" + mail + ", viewType="
				+ viewType + ", lng=" + lang + "]";
	}

	
	

	//mutatory
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}


	public int getViewType() {
		return viewType;
	}

	public void setViewType(int viewType) {
		this.viewType = viewType;
	}

	
	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	
	
	
	
	
	
	
	
}