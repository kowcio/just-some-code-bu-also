package grow.beans;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Named
public class UserBoImpl implements UserBo{
	 
	@PostConstruct
	public void start() {
		System.out.println("dziala user bean impl");
	}
	
	public String getMessage() {
		System.out.println("UserBoIml fired");
		return "JSF 2 + Spring Integration - msg from the bean";
 
	}
 
}