package grow.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Named ;//for bean declaration
import javax.inject.Inject; //for injection

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import javax.enterprise.context.RequestScoped;
//import javax.enterprise.context.SessionScoped;



@Named
public class UserBean {

	String name;
	
	@Inject
	private UserBoImpl userBo;
		
	@PostConstruct	
	public void start() {System.out.println("dziala user bean");	}
	
public String printMsgFromSpring() {	return userBo.getMessage();}
public String printFromBean() 	   {	return "printed from UserBean";}

	

public void setUserBo(UserBoImpl userBo) {
	this.userBo = userBo;
}
public UserBo getUserBo() {
	return userBo;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}
 







}