package grow.controllers_v2;


import grow.controllers.Utils;
import grow.daos.CommentDAO;
import grow.daos.GrowDao;
import grow.daos.UserDAO;
import grow.entities.Comment;
import grow.entities.Grow;
import grow.entities.User;
import grow.mail.MailSendCommentApproval;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;


/**
 * Main controller.
 * @author TalentLab1
 *
 */

@Controller
public class MainController2 {

	@Autowired
	public MessageSource msgSrc;  
    
		  public void setMsgSrc(MessageSource msgSrc) {  
		     this.msgSrc = msgSrc;  
		  } 	
		  
	Utils util = new Utils();	
	Logger log = Logger.getLogger("Iter4Logger");
	
	
	
	
	
	
	/**
	 * 	MAIN INDEX OF THE SITE 
	 * 	
	 */
	@RequestMapping(value="/v2",method= RequestMethod.GET)
	public ModelAndView indexV2(
			@ModelAttribute("user") User user,
			@ModelAttribute("grow") Grow grow, 
			Principal principal,
			BindingResult result,
			ModelAndView mav,
			HttpServletRequest request
			)
	{
			      mav.setViewName("index");
	  
			      
			      
			      
			      
			      
	     	return mav;
	} // end main index
	
	
	
	
	
	
	
	
	

	}
	
	
