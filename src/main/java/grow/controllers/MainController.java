package grow.controllers;


import grow.daos.CommentDAO;
import grow.daos.GrowDao;

import grow.daos.UserDAO;
import grow.entities.Comment;
import grow.entities.Grow;

import grow.entities.User;
import grow.mail.MailSendCommentApproval;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;


/**
 * Main controller.
 * @author TalentLab1
 *
 */

@Controller
public class MainController {

	@Autowired
	public MessageSource msgSrc;  
    
		  public void setMsgSrc(MessageSource msgSrc) {  
		     this.msgSrc = msgSrc;  
		  } 	
		  
	Utils util = new Utils();
	
	Logger log = Logger.getLogger("Iter4Logger");
	
	
	
	
	@RequestMapping(value="/index2",method= RequestMethod.GET)
	public ModelAndView indexWiqwethoutLogin(
			@ModelAttribute("user") User user,
			@ModelAttribute("grow") Grow grow, 
			Principal principal,
			BindingResult result,
			ModelAndView mav,
			HttpServletRequest request
			)
	{
			
		boolean isUserAuth = false;
		
	      if (isUserAuth == true ){
	    	  
	      }
	      else {
	    	  Grow gr_basic = Grow.createGrow();
	    	  mav.addObject("grow" , gr_basic);
				System.out.println(" LOADING INDEX  - submitted an INVALID username");
			      mav.setViewName("index2");
	     }
	     	return mav;
	} // end main index
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	//<//!-- JSF TEST >
	/**
	 * About url
	 */
		@RequestMapping(value="bean",method= RequestMethod.GET)
		public ModelAndView ejb(ModelAndView mav) 
		{   mav.setViewName("beans");
			return mav;
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
/**
 * Intercept URL for loggin part in spring security		<br /> 
 * value={"/login","/logout","/loginFAIL"}				<br />
 * basic url after login is the login requested 		<br />
 * @param mav
 * @return
 */
	@RequestMapping(value={"/login","/logout","/loginFAIL"},method= RequestMethod.GET)
	public ModelAndView indexWithLoginFormn(ModelAndView mav) 
	{	//maybe add a msg for each url 
		Grow gr_basic = Grow.createGrow();
   	  	mav.addObject("grow" , gr_basic);
		mav.setViewName("index2");
			return mav;
	}
	
	
	/**
	 * 	MAIN INDEX OF THE SITE 
	 * 
	 * @param user - form from spring security 
	 * @param principal -  form from spring security
	 * @param result 
	 * @param mav 
	 * @return View of the main site.
	 */
	
	@RequestMapping(value="/",method= RequestMethod.GET)
	public ModelAndView indexWithoutLogin(
			@ModelAttribute("user") User user,
			@ModelAttribute("grow") Grow grow, 
			ModelAndView mav
			)
	{
			
				 System.out.println(" LOADING INDEX  - submitted an INVALID username");
			      mav.setViewName("index2");
	    
	     	return mav;
	} // end main index
	
	
	/**
	 * About url
	 */
		@RequestMapping(value="about",method= RequestMethod.GET)
		public ModelAndView about(ModelAndView mav) 
		{   mav.setViewName("about");
			return mav;
		}

		
		
		
		/**
		 * RETURN info with currently loged in user 
		 */
		
		@RequestMapping(value="getUserInfo",method= RequestMethod.GET)
		@ResponseBody
		public String getUserInfoLogindAndID()				 
		{   
						
		//extract it somewhere else
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			
	    String name = "Name" ; //auth.getName(); //get logged in username
	    UserDAO udao = new UserDAO();
	    User user = udao.getUserByString(name);
	    Integer id = util.safeLongToInt( user.getId() );
	    log.info("Logged user id = " + id);
	    
	    if (user.equals("anonymouseUser") || id==0)
	    return "Welcome <span id=\"loggedUsername\">Unnamed</span>"+
	    		" <span id=\"loggedUserID\" class=\"hidden\">0</span>";
	    else
	    	return "Welcome <span id=\"loggedUsername\">"+name+"</span>"+
	    		" <span id=\"loggedUserID\" class=\"hidden\">"+id+"</span>";
		
		}
		
		
		
		
		
		
		
		
	//		METHODS
	
		
	
	/**
	 * Add posts list  "PostsList" to the given ModelAndView object and returns it.
	 * 
	 * @param mav
	 * @return mav   mav.addObject("PostsList" , pl );
	 */
	private static ModelAndView addCommentsList(ModelAndView mav){
		List<Comment> pl = new CommentDAO().getAllComments();
		if (pl.size()<=0)				mav.addObject("postEMPTY" , "postListIsNull");
		else 							mav.addObject("PostsList" , pl );
		return mav;
	}
	
	/*
	 * Returning the grows list for the logged user.
	 */
	private ArrayList<Grow> getUserGrowsList() {
		
	  	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName(); //get logged in username
		UserDAO udao = new UserDAO();
		User user = udao .getUserByString(name);
		Integer id = util.safeLongToInt( user.getId() );
		ArrayList<Grow> gl = new ArrayList<Grow>();
		GrowDao gdao = new GrowDao();
    	gl = gdao.getGrowsByUserID(id);
    	
    	return gl;
	}
	
	
	
	
	
	
	
	
	

	}
	
	
