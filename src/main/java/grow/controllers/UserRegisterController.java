package grow.controllers;


import grow.daos.HashMachine;
import grow.daos.HibGetDBSession;
import grow.daos.UserDAO;
import grow.entities.User;
import grow.user.UserDaoImpl;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;



/** 
 * User register Controller.
 * @author TalentLab1
 *
 */

@Controller
public class UserRegisterController {

	
	Logger log = Logger.getLogger("Iter4Logger");

	
	
	@RequestMapping(value="/register/reguser", method = RequestMethod.GET)
	public ModelAndView registerUser(
			@ModelAttribute("user") User newuser, 
			BindingResult result, SessionStatus status)
	{
			ModelAndView mav = new ModelAndView();
			mav.setViewName("/register/register_form");
			mav.addObject("user", newuser);
			log.info("/register/reguser controller for url fired "+	newuser.toString());
			return mav;
}	
	
	
	
	
	//rejestracja usera 
	@RequestMapping(value="/register/reguser", method = RequestMethod.POST)//link url
	public ModelAndView registerUserPOST(
			User newuser, ModelAndView mav ,
			BindingResult result
			) {
		
		mav.setViewName("/register/register_form");
        if (result.hasErrors()){
			log.info(" User add errors ");
			result.getAllErrors();
			mav.addObject("user",new User());
			return mav;
			}
		else{
		
			log.info(" User = "+newuser.getLogin() + " / "+newuser.getPasswd());
			HibGetDBSession fabrykaHibernejta = new HibGetDBSession();
			UserDaoImpl udao = new UserDaoImpl(   fabrykaHibernejta.getAnnotationsSessionFactory()	);
//error z rzucaniem wyjatkow, przyjrzec sie blize j !! !! !! 
			UserDAO udao2 = new UserDAO();
			
			HashMachine hm = new HashMachine();
			newuser.setPasswd( hm.hashThePass_USE_THIS(   newuser.getPasswd()	) );
			newuser.setEnabled(1);

			
				try {
					log.info("Generated "+newuser.getPasswd().length()+" characters hash");
					udao.save(newuser);
					newuser = new User();
						
				} 
				catch ( ConstraintViolationException e2 ){
					mav.addObject( "allNotOK"	,	"User name \""+newuser.getLogin()+"\" taken.");
					log.info("getCause = "+e2.getCause());
					e2.printStackTrace();
					mav.addObject("user",new User());
					return mav;
				}
			catch (Exception e ) {
				log.info("getCause = "+e.getCause());
				mav.addObject( "allNotOK"	,	"Error while persisting to the DB.");
				e.printStackTrace();
				mav.addObject("user",new User());
				return mav;

			}
		
		mav.addObject( "allOK"	,	"All ok ! New user \""+newuser.getLogin()+"\" saved !");
		mav.addObject("user",new User());
		return mav;
		}
		
	}

}
	
	
	
	