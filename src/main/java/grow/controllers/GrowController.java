package grow.controllers;


import java.util.ArrayList;

import grow.daos.GrowDao;
import grow.daos.UserDAO;
import grow.entities.Grow;
import grow.entities.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.http.HttpRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Grow controller.
 * @author Kowalski Piotr
 *
 */
@Controller
public class GrowController {

	@Autowired
	public MessageSource msgSrc;  
    
		  public void setMsgSrc(MessageSource msgSrc) {  
		     this.msgSrc = msgSrc;  
		  } 	
		  
	Utils util = new Utils();
	Logger log = Logger.getLogger("Iter4Logger");
	
	
	
	/**
	 *  SAVING THE GROW 
	 */
	
	@RequestMapping(value="/growadd",method= RequestMethod.POST)
	public ModelAndView postAddGETForm(
			@ModelAttribute("grow") @Valid Grow grow, 
			BindingResult result,
			ModelAndView mav,
			HttpServletRequest request,
			GrowDao gdao 
			) {
	    try {
			Authentication a = SecurityContextHolder.getContext().getAuthentication();
			UserDetails currentUserDetails = (UserDetails) a.getPrincipal();
			UserDAO udao = new UserDAO();
			User user = udao.getUserByString(	currentUserDetails.getUsername()	);
			
			grow.setUser_id(user.getId());
			
			System.out.println("user id =  "+ user.getId()); 
			System.out.println(" ==  > "+grow.toString() );
			//retrieve grow id 
			HttpSession session = request.getSession();
			//int growid = (Integer) session.getAttribute("growid") 	;
			gdao.saveOrUpdateGrow(grow);
			//mav.setViewName("redirect:/");
			mav.setViewName("index2");
			//add the user grows
	    	  mav.addObject("growsList", util.getUserGrowsList());
		} catch (Exception e) {
			log.info(e.getCause());
		}
		return mav;
	}
	
	
	
	
	
	/**
	 * Loading the grows for the user
	 */
	@RequestMapping(value="/growget",method= RequestMethod.GET)
	@ResponseBody
	public ArrayList<Grow> getUserGrows(
			GrowDao gdao,
			ArrayList<Grow> gl,
			HttpServletRequest request
			//@RequestParam("uid") int uid
			) {	
	    try {
	    	String uids = request.getParameter("uid");
	    	int uid = Integer.parseInt(uids);
	    	log.info("getting grows for user = " + uid);
	    	gl = gdao.getGrowsByUserID(uid);
	    	
		} catch (Exception e) {
			e.getCause();
			log.info("Cos sie wysypalo - /growadd");
			 gl.add(new Grow());
		}
		return gl;
	}
	
	/**
	 * Loading the grows for the user
	 */
	@RequestMapping(value="/addnextgrow",method= RequestMethod.POST)
	@ResponseBody
	public boolean addGrowForUsero(
			GrowDao gdao,
			Grow newGrow,
			ArrayList<Grow> gl,
			@RequestParam("uid") int uid,
			HttpServletRequest request
			) {
		Utils util = new Utils();
		//log.info("User id z utilsow =" + util.getUserNameIfAuthAndUserID().get(0) );
		//log.info("User id z utilsow =" + util.getUserNameIfAuthAndUserID().get(1) );
		//log.info("User id z utilsow =" + util.getUserNameIfAuthAndUserID().get(2) );
		
		//NIEZGADZA SIE po wylogowaniu i zalogowaniu na kogos innego, WTF ?! ?! 
		
		log.info("User id z utilsow =" + Integer.parseInt( util.getUserNameIfAuthAndUserID().get(2) ) 
				+ "user przekazany" +uid);
		
		  HttpSession session = request.getSession();
    	  int userid = util.safeLongToInt( (Long) session.getAttribute("userid") ) ;
    	  log.info("User from session = "+ userid);
		//if (Integer.parseInt( util.getUserNameIfAuthAndUserID().get(2) ) != uid) return false;
		
		
	    try {
	    	log.info("Adding new grow for user = " + uid);
	    	int size = (int)gdao.getGrowsByUserID(uid).size();
	    	log.info("Grows when adding = " + size);
	    	if ( size < 4 	) {	//why the hell <3 not 4 ? no idea ...
	    		newGrow = Grow.createGrow();
	    		newGrow.setUser_id((long) uid);
	    		gdao.saveGrow(newGrow);
	    		return true;
	    	}
	    	else {	
	    		return false;
	    	}
		} catch (Exception e) {
			log.info("Cos sie wysypalo - /addnextgrow");
			 gl.add(new Grow());
			 return false;
		}
	}
		
	
	
	
	
	/**
	 * Deleteing the grow for the user
	 */
	@RequestMapping(value="/delgrow",method= RequestMethod.POST)
	@ResponseBody
	public String delGrow(
			GrowDao gdao,
			Grow newGrow,
			ArrayList<Grow> gl,
			@RequestParam("gid") int gid
			) {	
		
		//if (checkOwner(gid) == false) return "Can`t delete not Your grow !";
		
	    try {
	    	gdao.deleteGrow(gid);
	    	return "Deleted grow ! (Hope it helped !)";

	    } catch (Exception e) {
			log.info("cant /delgrow , some error");
			e.getCause();
			return "Error deleting grow ;(";
		}
	}
		
	
	
	
	/**
	 * Loading the grow for the user
	 */
	@RequestMapping(value="/loadgrow",method= RequestMethod.GET)
	@ResponseBody
	public Grow delGrow(
			GrowDao gdao,
			@RequestParam("gid") int gid
			) {	
	    
		//if (checkOwner(gid) == false) return new Grow();
		
		try {
	    	return gdao.getGrowByID(gid);

	    } catch (Exception e) {
			log.info("cant load grow , some error");
			e.getCause();
			return new Grow();
		}

	}
	
	
	
	
	//METHODS
	
	
	/**
	 * Checking ownership of given grow(given gid) by user ,
	 * need to remake this with session attribut or something, 
	 * getting back first logged in while other loged in also, further tests needed !!
	 */
	
	public boolean checkOwner(int gid) {
		boolean approve = false;
		//log.info("Grow id to check = "+gid);
		for ( int i = 0 ; i < 4 ; i++) {
		 Grow grow = util.getUserGrowsList().get(i);
		 //log.info("Grow id from db = #" + giddb+ "# grow id to check = #"+gid+"#"); 
			 if (grow.getId() == gid )
				 return true;
			 else approve = false;
			 
		}
		return approve;
		
		
		
	}
}