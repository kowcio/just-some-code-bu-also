package grow.controllers;

import grow.daos.CommentDAO;
import grow.daos.GrowDao;

import grow.daos.UserDAO;
import grow.entities.Comment;
import grow.entities.Grow;

import grow.entities.User;
import grow.mail.MailSendCommentApproval;

import java.security.Principal;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;


@Controller
public class ContactController {

	@Autowired
	public MessageSource msgSrc;  
    
		  public void setMsgSrc(MessageSource msgSrc) {  
		     this.msgSrc = msgSrc;  
		  } 	
		  
	Utils utl = new Utils();
	
	Logger log = Logger.getLogger("Iter4Logger");
	
	
	
	
	
	
	/**
	 *Form to add a comment and send a mail
	 *@Valid is geting us 400, nothing to validate or wrong data ? 
	 *(if would validate the constructor vs annotations should be error at start ? 
	 *without works ;)
	 * @return
	 */
	@RequestMapping(value="contact",method= RequestMethod.GET )
	public ModelAndView contactForm(
			@ModelAttribute("comment") Comment newComment,
			ModelAndView mav
			) {
		mav.addObject("comment", newComment);
		mav.setViewName("contact");
		return mav;
}
	/**
	 * Post for the contact info
	 */
	@RequestMapping(value="contact/add",method= RequestMethod.POST )
	public ModelAndView contactFormAddPost(
			@ModelAttribute("comment") @Valid  Comment newComment,
			BindingResult result,
			ModelAndView mav,
			CommentDAO cdao,
			MailSendCommentApproval msca,
			Locale locale
			) {
		mav.setViewName("contact");

		if (result.hasErrors()){
			log.info(" Comment add errors ");
			//<!-- NEED TO BE FOR VALIDATION !! -->
			mav.addObject(result.getModel());
			return mav;
			}
		else{
			
		log.info("Adding contact no binding errors.");
		
		//load messageSource
		msca.sendCommentOKMail(msgSrc,newComment);
		cdao.saveComment(newComment);
		//add msg mail send OK
		String mailSendOK = msgSrc.getMessage("mail.send.ok"	,	null,locale );
		System.out.println(mailSendOK);
		mav.addObject("mailOK", mailSendOK);
		mav.addObject("comment", new Comment());
		//send email

		}
		return mav;
}	
	
	
	/**
	 *Url including the list of comments for the site,
	 *almost as a guest book.
	 *Does not work in site after POST, when this needs get ? 
	 */
	@RequestMapping(value="{/show/comments, /contact/comments}",method= RequestMethod.GET ,  produces="text/html")
	@ResponseBody
	public ModelAndView show10PostsStartingFromFirst(
			ModelAndView mav,
			CommentDAO cdao
			) {
		mav.setViewName("comments/singleCommentsListPage");		
		List<Comment> cp = cdao.getAllComments();
		mav.addObject("commentList",cp);
		return mav;
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
