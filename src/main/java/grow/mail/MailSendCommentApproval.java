package grow.mail;
import java.util.Locale;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

import org.springframework.context.MessageSource;
import grow.entities.Comment;
 


public class MailSendCommentApproval {

	
	
	
	
	
	 public String sendCommentOKMail(MessageSource msgSrc, Comment comment ){
		Locale locale = new Locale("eng");
		
		 
String nick = comment.getNick();
String commentContent = comment.getContent();
			
///resources/lang/ *.properties
String subject = msgSrc.getMessage("mail.comment.ok.subject"	, 	null,null );
String content = msgSrc.getMessage("mail.comment.ok.content"	,	null,null );
		 
	//globalProps.properties
		 String from = msgSrc.getMessage("mail.comment.from" ,null, null );
		 String to = msgSrc.getMessage("mail.comment.to" ,null, null );
		 
		 final String username = msgSrc.getMessage("mail.comment.username" ,null, null );
		 final String password = msgSrc.getMessage("mail.comment.password" ,null, null );
		  
		 String auth 		= msgSrc.getMessage("mail.smtp.auth" ,null, null );
		 String tlsenabled 	= msgSrc.getMessage("mail.smtp.starttls.enable" ,null, null );
		 String port 		= msgSrc.getMessage("mail.smtp.port" ,null, null );
		 String host 		= msgSrc.getMessage("mail.smtp.host" ,null, null );
		
		 // Set the system properties
		    Properties properties = System.getProperties();
		    properties.put("mail.smtp.auth", auth);
		    properties.put("mail.smtp.starttls.enable", tlsenabled);
		    properties.put("mail.smtp.port", port);
		    properties.put("mail.smtp.host", host);
		    
			  // Get the default Session object.

			Session session = Session.getInstance(properties,
					  new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username, password);
						}
					  });

		    // Create a default MimeMessage object.
		  MimeMessage message = new MimeMessage(session);

		  try {
			// Set the RFC 822 "From" header field using the
			  // value of the InternetAddress.getLocalAddress method.
			  message.setFrom(new InternetAddress(from));

			  // Add the given addresses to the specified recipient type.
			  message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

			  // Set the "Subject" header field.
			  message.setSubject(subject);

			  // Sets the given String as this part's content,
			  // with a MIME type of "text/plain".
		        message.setContent("<h5><br />"+content+"<br /></h5>", "text/html");
			  // Send message
			  Transport.send(message);
			  System.out.println("Mail message Send.....");

		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		  
	 return "ok";
	 }
		}